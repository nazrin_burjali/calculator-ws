package com.ws.calculator.controller;

import com.ws.calculator.soap.CalculatorSoap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorRestController {
    @Autowired
    private CalculatorSoap calculatorSoap;

    @PostMapping("/add")
    public int add(@RequestParam(name = "a") int intA,
                   @RequestParam(name = "b") int intB){
        return calculatorSoap.add(intA, intB);
    }

    @PostMapping("/subtract")
    public int subtract(@RequestParam(name = "a") int intA,
                   @RequestParam(name = "b") int intB){
        return calculatorSoap.subtract(intA, intB);
    }

    @PostMapping("/divide")
    public int divide(@RequestParam(name = "a") int intA,
                        @RequestParam(name = "b") int intB){
        return calculatorSoap.divide(intA, intB);
    }

    @PostMapping("/multiply")
    public int multiply(@RequestParam(name = "a") int intA,
                      @RequestParam(name = "b") int intB){
        return calculatorSoap.divide(intA, intB);
    }

}
